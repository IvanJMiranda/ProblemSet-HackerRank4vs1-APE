import java.util.Scanner;

/**
 *@Author Ivan J. Miranda
 *STATUS: UNSOLVED. 
 *
 *=EUGENE AND BIG NUMBER=
 *
 *Problem Statement:
 *Eugene must do his homework, but he is struggling. 
 *He has three integer numbers: A, N, M. He writes number A on the board N 
 *times in a row. Let's call the resulting big number X. Help Eugene find X modulo M. 
 *
 *Input Format:
 *First line contains T, the number of testcases. 
 *Each testcase contains three numbers: A, N, M separated by a single space. 
 *
 *Constraints: 
 *1 <= T <= 200 
 *0 <= A <= 1000 (1e3) 
 *0 < N < 1000000000000 (1e12) 
 *1 < M < 1000000000 (1e9) 
 *
 *Output Format:
 *Print the required answer for each testcase in a new line.
 *
 *Sample Input:
 *2  
 *12 2 17  
 *523 3 11  
 *
 *Sample Output:
 *5
 *6
 *
 *Explanation:
 *First testcase:
 *A = 12
 *N = 2
 *X = 1212
 *1212 modulo 17 = 5
 *
 *Second testcase:
 *A = 523
 *N = 3
 *X = 523523523
 *523523523 modulo 11 = 6
 */

public class ModuloEugene 
{
	public static void main(String[] args) 
	{
		@SuppressWarnings("resource")
		Scanner userInput = new Scanner(System.in);
		
		int t = Integer.parseInt(userInput.nextLine());
		
		for (int i = 0; i < t; i++)
		{
			String strOriginal = userInput.next();
			long original = Long.parseLong(strOriginal);
			long times = Long.parseLong(userInput.next());
			long modulus = Long.parseLong(userInput.next());
			long pseudoResult1 = 0;
			long pseudoResult2 = 0;
			long pseudoResult3 = 0;
			long pseudoResult4 = 0;
						
			for (long j = 0; j < times/4; j++) 
			{
				pseudoResult1 += original%modulus;
				original *= Math.pow(10, strOriginal.length());
			}
			
			for (long j = (times/4); j < 2*(times/4); j++) 
			{
				pseudoResult2 += original%modulus;
				original *= Math.pow(10, strOriginal.length());
			}
			
			for (long j = (times/2); j < (3)*(times/4); j++) 
			{
				pseudoResult3 += original%modulus;
				original *= Math.pow(10, strOriginal.length());	
			}
			for (long j = (3)*(times/4); j < times; j++) 
			{
				pseudoResult4 += original%modulus;
				original *= Math.pow(10, strOriginal.length());	
			}
			
			System.out.println((pseudoResult1 + pseudoResult2 + pseudoResult3 + pseudoResult4)%modulus);	
			
		}
	}
}
